#!/bin/sh
if [ -f "settings.sh" ]; then
    source ./settings.sh
fi

if [ -z "$PREFIX" ]; then
    echo "Please set the environment variable \$PREFIX to the installation destination. User settings.sh for this"
    exit 1
fi 
export PATH=$PREFIX/bin:$PATH
export LD_LIBRARY_PATH=$PREFIX/lib:$PREFIX/lib64:$LD_LIBRARY_PATH
THIS=$PWD
set -e
set -o verbose

function installPackage() {
  if [ ! -e "$1" ]; then
    $1
    cd $THIS
    touch $1
  else
    echo $1 already installed
  fi
}

function opencv() {
  wget http://downloads.sourceforge.net/project/opencvlibrary/opencv-unix/2.4.10/opencv-2.4.10.zip
  unzip opencv-2.4.10.zip
  rm opencv-2.4.10.zip
  cd opencv-2.4.10
  mkdir build
  cd build
  cmake -D CMAKE_BUILD_TYPE=RELEASE -D CMAKE_INSTALL_PREFIX=$PREFIX ../
  make $MAKE_ARGS
  make install
}
installPackage opencv

function boost() {
  wget http://downloads.sourceforge.net/project/boost/boost/1.57.0/boost_1_57_0.tar.gz
  tar xzf boost_1_57_0.tar.gz
  rm boost_1_57_0.tar.gz
  cd boost_1_57_0
  ./bootstrap.sh && ./b2 install --prefix=$PREFIX
}
installPackage boost

function liblinear() {
  wget http://www.csie.ntu.edu.tw/~cjlin/liblinear/liblinear-1.96.tar.gz
  tar xzf liblinear-1.96.tar.gz
  rm liblinear-1.96.tar.gz
  cd liblinear-1.96
  make $MAKE_ARGS && make $MAKE_ARGS lib && install -m 755 linear.h $PREFIX/include && install -m 755 liblinear.so.2 $PREFIX/lib
  ln -sf $PREFIX/lib/liblinear.so.2 $PREFIX/lib/liblinear.so
}
installPackage liblinear

function glog() {
  wget https://google-glog.googlecode.com/files/glog-0.3.3.tar.gz
  tar xzf glog-0.3.3.tar.gz
  rm glog-0.3.3.tar.gz
  cd glog-0.3.3
  ./configure --prefix=$PREFIX && make $MAKE_ARGS && make install
}
installPackage glog

function gflags() {
  wget https://github.com/schuhschuh/gflags/archive/v2.1.1.tar.gz
  tar xzf v2.1.1.tar.gz
  rm v2.1.1.tar.gz
  cd gflags-2.1.1
  mkdir build
  cd build
  cmake -PREFIXD CMAKE_INSTALL_PREFIX=$PREFIX -D CMAKE_CXX_FLAGS=-fPIC ../
  make $MAKE_ARGS
  make install
}
installPackage gflags 

function protobuf() {
  wget https://github.com/google/protobuf/releases/download/v2.6.1/protobuf-2.6.1.tar.gz
  tar xzf protobuf-2.6.1.tar.gz
  rm protobuf-2.6.1.tar.gz
  cd protobuf-2.6.1/
  ./configure --prefix=$PREFIX && make $MAKE_ARGS && make install
}
installPackage protobuf

function leveldb() {
  git clone https://github.com/google/leveldb.git
  cd leveldb
  make $MAKE_ARGS
  cd include
  cp -r leveldb $PREFIX/include
  cd ../
  install -m 755 libleveldb.a $PREFIX/lib
  install -m 755 libleveldb.so.1.18 $PREFIX/lib
  ln -fs $PREFIX/lib/libleveldb.so.1.18 $PREFIX/lib/libleveldb.so
  ln -fs $PREFIX/lib/libleveldb.so.1.18 $PREFIX/lib/libleveldb.so.1
}
installPackage leveldb

function snappy() {
  git clone https://github.com/google/snappy.git snappy.compile
  cd snappy.compile
  ./autogen.sh
  ./configure --prefix=$PREFIX && make $MAKE_ARGS && make install
}
installPackage snappy

function hdf() {
  wget http://www.hdfgroup.org/ftp/HDF5/current/src/hdf5-1.8.14.tar.gz
  tar xzf hdf5-1.8.14.tar.gz
  rm hdf5-1.8.14.tar.gz
  cd hdf5-1.8.14
  ./configure --prefix=$PREFIX && make $MAKE_ARGS && make install
}
installPackage hdf

function mdb() {
  git clone https://gitorious.org/mdb/mdb.git
  cd mdb/libraries/liblmdb
  make $MAKE_ARGS
  install -m 755 liblmdb.so $PREFIX/lib
  install -m 755 liblmdb.a $PREFIX/lib
  install -m 755 lmdb.h $PREFIX/include
  install -m 755 midl.h $PREFIX/include
}
installPackage mdb 

function openblas() {
  wget http://github.com/xianyi/OpenBLAS/tarball/v0.2.13
  tar xzf v0.2.13
  rm v0.2.13
  cd xianyi-OpenBLAS-aceee4e
  make $MAKE_ARGS
  make install PREFIX=$PREFIX
}
installPackage openblas

function zeromq() {
  wget http://download.zeromq.org/zeromq-4.0.5.tar.gz
  tar xzf zeromq-4.0.5.tar.gz
  cd zeromq-4.0.5
  ./configure --prefix=$PREFIX
  make $MAKE_ARGS
  make install
}
installPackage zeromq

function cppzeromq() {
  git clone https://github.com/zeromq/cppzmq.git
  cd cppzmq
  install -m 755 zmq.hpp $PREFIX/include 
}
installPackage cppzeromq

function caffe() {
  git clone git@github.com:kencoken/caffe.git caffe.compile
  cd caffe.compile
  git fetch origin
  git checkout --track origin/set-input-count
  cp Makefile.config.example Makefile.config
  sed -i -e "s#INCLUDE_DIRS := \$(PYTHON_INCLUDE) /usr/local/include#INCLUDE_DIRS := \$(PYTHON_INCLUDE) /usr/local/include $PREFIX/include#" Makefile.config
  sed -i -e "s#LIBRARY_DIRS := \$(PYTHON_LIB) /usr/local/lib /usr/lib#LIBRARY_DIRS := \$(PYTHON_LIB) /usr/local/lib /usr/lib $PREFIX/lib $PREFIX/lib64#" Makefile.config
  sed -i -e "s/# CPU_ONLY := 1/CPU_ONLY := 1/" Makefile.config
  sed -i -e "s/BLAS := atlas/BLAS := open/" Makefile.config  
  make $MAKE_ARGS all
  # the last three tests fail at the moment although they don't affect visor... leaving testing out for now
  #make test
  #make runtest
  install -m 0755 build/lib/libcaffe.so $PREFIX/lib
  rm -rf $PREFIX/include/caffe  
  cp -r include/caffe $PREFIX/include/caffe
  mkdir -p $PREFIX/include/caffe/proto/
  cp -r build/src/caffe/proto/*.h $PREFIX/include/caffe/proto/
}
installPackage caffe

function cppnetlib() {
  git clone https://github.com/kencoken/cpp-netlib.git
  cd cpp-netlib
  git checkout 0.11-devel
  mkdir build
  cd build
  cmake -D  CMAKE_INSTALL_PREFIX=$PREFIX ../ 
  make $MAKE_ARGS
  make install
}
installPackage cppnetlib

function cmake() {
  wget http://www.cmake.org/files/v3.2/cmake-3.2.2.tar.gz
  tar xvzp -f cmake-3.2.2.tar.gz
  rm cmake-3.2.2.tar.gz
  mv cmake-3.2.2 cmake-compile
  cd cmake-compile
  ./bootstrap --prefix=$PREFIX
  make $MAKE_ARGS
  make install
}
installPackage cmake

function setup_env() {
cat > setup_env.sh <<HERE
export PATH=$PREFIX/bin:\$PATH
export CPLUS_INCLUDE_PATH=$PREFIX/include:\$CPLUS_INCLUDE_PATH
export LD_LIBRARY_PATH=$PREFIX/lib:$PREFIX/lib64:\$LD_LIBRARY_PATH
export LIBRARY_PATH=$PREFIX/lib:$PREFIX/lib64:\$LIBRARY_PATH
export BOOST_ROOT=$PREFIX
export CMAKE_PREFIX_PATH=$PREFIX
export MKL_NUM_THREADS=1
HERE
}
installPackage setup_env
