CPUVISOR_SRV Installation script
==================================

This projects helps installing the dependencies of cpuvisor-srv 
(``https://github.com/kencoken/cpuvisor-srv``) under one prefix directory,
in the user space (i.e. no root rights required).

Please proceed as follows:
   $ cp settings.sh.template settings.sh
   $ $EDITOR settings.sh
   $ ./install_cpuvisor_dependencies.sh

The installation process creates a file ``setup_env.sh`` which sets the
required environment variables.
